import pycurl
import pandas as pd
from io import BytesIO
from bs4 import BeautifulSoup
from urllib import parse
from datetime import datetime
from utility import computeAge, computeDate, extractAddress


buildingTypes = {
    "公寓": "apartment",
    "華廈": "elevatorBuilding",
    "透天厝": "detachedHouse",
    "辦公": "business",
    "店面": "store",
    "工廠": "factory",
    "廠辦": "factoryOffice",
    "純土地": "land",
    "純車位": "parking",
    "其他": "other",
}

countyNumbers = {
    0: "臺北市",
    2: "新北市",
    9: "彰化縣",
    12: "嘉義市",
    17: "屏東縣",
}


class HousePriceScraperRakuya:
    def __init__(self) -> None:
        self.pastDeals = []
        self.today = datetime.today()
        self.year = self.today.year
        self.month = self.today.month

        pass

    def setURL(
        self,
        base: str,
        citycode: int,
        addressKeyword: str,
        zipcode: int = None,
        sellTypes: list[str] = [],
        closeYear: str = "",
        page: str = "",
    ):
        """Suppose that address is in Chinese characters."""

        # Initialize objects.
        self.b_obj = BytesIO()
        self.crl = pycurl.Curl()

        # Set basic url components.
        baseUrl = base
        citycodeUrl = f"city={citycode}"
        if zipcode is not None:
            zipcodeUrl = f"&zipcode={zipcode}"
        else:
            zipcodeUrl = ""
        addressUrl = f"&search=route&keyword={parse.quote(addressKeyword)}"

        # Whether to filter based on sell_type.
        # 公寓：apartment
        # 華廈：elevatorBuilding
        # 透天：detachedHouse
        # 辦公：business
        # 店面：store
        # 工廠：factory
        # 廠辦：factoryOffice
        if len(sellTypes) == 0:
            selltypeUrl = ""
        elif len(sellTypes) == 1:
            selltypeUrl = f"&sell_type={buildingTypes[sellTypes[0]]}"
        elif len(sellTypes) >= 2:
            # sell_type=apartment%2CelevatorBuilding
            selltypeUrl = f"&sell_type={buildingTypes[sellTypes[0]]}"
            for i in range(1, len(sellTypes)):
                selltypeUrl += f"%2C{buildingTypes[sellTypes[i]]}"

        # Whether to filter based on deal date.
        if closeYear == "":
            yearUrl = "&sort=12"
        else:
            yearUrl = f"&closedate={closeYear}&sort=12"

        # If there are many results, we can go to specific page.
        if page == "":
            pageUrl = ""
        else:
            pageUrl = f"&page={page}"

        # Append url components to get the final url.
        finalUrl = (
            baseUrl
            + citycodeUrl
            + zipcodeUrl
            + addressUrl
            + selltypeUrl
            + yearUrl
            + pageUrl
        )

        # Set url
        self.crl.setopt(self.crl.URL, finalUrl)
        self.sellTypes = sellTypes
        self.county = countyNumbers[citycode]
        # Note down addressKeyword used in search because these are most likely
        # non-traditional addresses (without 路、街、道).
        self.addressKeyword = addressKeyword

    def scrapeText(self):
        self.crl.setopt(self.crl.WRITEDATA, self.b_obj)
        self.crl.perform()
        # Close crl after getting text.
        self.crl.close()

        get_body = self.b_obj.getvalue()
        html = get_body.decode("utf8")
        soup = BeautifulSoup(html, features="html.parser")

        text = soup.get_text()
        lines = [line.strip() for line in text.splitlines()]
        chunks = [phrase.strip() for line in lines for phrase in line.split("  ")]
        # Exclude empty strings.
        validChunks = [chunk for chunk in chunks if chunk]

        # Find past deals found in results.
        pastDealIdxStart = []
        pastDealIdxEnd = []
        for i, chunk in enumerate(validChunks):
            if "/" in chunk and chunk[-1] != "樓" and chunk[-1] != "坪":
                try:
                    year = float(chunk.split("/")[0])
                    pastDealIdxStart.append(i)
                except ValueError:
                    pass
            if "歷史交易" in chunk:
                pastDealIdxEnd.append(i)

        if len(pastDealIdxStart) > 0:
            pastDeals = [
                validChunks[pastDealIdxStart[i] : pastDealIdxEnd[i] + 1]
                for i in range(len(pastDealIdxStart) - 1)
            ]
        else:
            pastDeals = []

        return pastDeals

    def appendPastDeals(self, pastDeals):
        self.pastDeals += pastDeals

    def getHouseInformation(self):
        cols = [
            "Address",
            "Age",  # Age at deal.
            "TransferLevel",
            "TotalFloor",
            "單價元平方公尺",
            "建物型態",
            "主要用途",
            "DealYearMonth",
            "DealYear",
            "建築完成年月",
            "Area",
            "County",
            "鄉鎮市區",
            "Village",
            "Street",
            "Section",
        ]
        # Initialize houses for this search.
        houseData = {}
        for col in cols:
            houseData[col] = []

        n0 = len(self.pastDeals)
        for i in range(n0):
            house = self.pastDeals[i]
            # Initialize info of this house
            d = {}
            for col in cols:
                d[col] = 0

            for j, x in enumerate(house):
                if "年" in x:
                    d["DealYearMonth"] = house[0]
                    d["Address"] = self.county + house[1]
                    d["Age"] = float(x[:-1])
                    ym = d["DealYearMonth"].split("/")
                    builtYear, builtMonth = computeDate(
                        float(ym[0]), float(ym[1]), -1 * float(x[:-1])
                    )
                    d["建築完成年月"] = str(builtYear) + "/" + str(builtMonth)
                    d["DealYear"] = (
                        int(ym[0]) - 1911
                    )  # In 民國 for compatibility with governmental data.

                elif x in buildingTypes.keys():
                    d["建物型態"] = x

                elif x == "坪":
                    d["Area"] = float(house[j - 1])

                elif x == "萬/坪":
                    d["單價元平方公尺"] = str(
                        float(house[j - 1]) * 1e4 / 3.3
                    )  # Convert to NTD/m^2.

                elif "樓" in x and "/" in x:
                    tt = x.split("/")
                    if "," in tt[0]:
                        tl = tt[0].split(",")[0]
                        try:
                            d["TransferLevel"] = int(tl)
                        except ValueError:
                            d["TransferLevel"] = -1
                    else:
                        try:
                            d["TransferLevel"] = int(tt[0])
                        except ValueError:
                            d["TransferLevel"] = -1
                    try:
                        d["TotalFloor"] = int(tt[1][:-1])
                    except ValueError:
                        d["TotalFloor"] = -1

            # Only keep those with age available.
            if (
                d["Age"] != ""
                and d["建物型態"] not in ["純車位", "純土地"]
                and d["單價元平方公尺"] != ""
            ):
                # Set purpose accordingly.
                if d["建物型態"] in ["公寓", "華廈", "透天厝", "電梯大樓"]:
                    d["主要用途"] = "住家用"
                elif d["建物型態"] == "辦公" or d["建物型態"] == "廠辦":
                    d["主要用途"] = "辦公室"
                elif d["建物型態"] == "店面":
                    d["主要用途"] = "商業用"
                # else:
                #     print(f'Unknown Type: {d["建物型態"]}')
                #     print(house)

                # Set building typeaccordingly.
                if d["TotalFloor"] > 0 and d["TotalFloor"] <= 5:
                    d["建物型態"] = "公寓(5樓含以下無電梯)"
                elif d["TotalFloor"] <= 10 and d["TotalFloor"] > 5:
                    d["建物型態"] = "華廈(10層含以下有電梯)"
                elif d["TotalFloor"] > 10:
                    d["建物型態"] = "住宅大樓(11層含以上有電梯)"

                county, district, village, street, section = extractAddress(
                    d["Address"], "all"
                )
                d["County"] = self.county
                d["鄉鎮市區"] = district
                d["Village"] = village
                d["Street"] = self.addressKeyword
                d["Section"] = section

                # Eg. 南勢一街 will appear in search with 南勢.
                if street != "" and street != self.addressKeyword:
                    continue

                # Append info to each column.
                for key, val in d.items():
                    houseData[key].append(val)

        self.houses = pd.DataFrame(data=houseData, columns=cols)

    def saveHousesToFile(self, fileName: str, years: list[int]):
        data = self.houses
        for year in years:
            sub = data[data["DealYear"] == year]
            sub.to_csv(f"./data/{fileName}_{str(year)}.csv", index=False)
