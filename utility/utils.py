import pandas as pd
from utility.address import isNonTraditionalAddress


def computeAge(yearFrom, monthFrom, yearTo, monthTo):
    diffY = yearTo - yearFrom
    diffM = monthTo - monthFrom
    if diffM < 0:
        diffM += 12
        diffY -= 1

    return diffY + diffM / 12.0


def computeDate(yearFrom, monthFrom, duration):
    """
    duration is in unit of year.
    """
    yearAdd = round(duration, 0)
    monthAdd = duration - yearAdd

    yearTo = yearFrom + yearAdd
    monthTo = monthFrom + monthAdd
    if monthTo > 12:
        monthTo -= 12
        yearTo += 1

    return int(yearTo), int(monthTo)


def addGovernmentalInformation(data: pd.DataFrame) -> pd.DataFrame:
    """
    This is ad hoc to make data consistent with govermental data and be used
    in filtering for tbrain_sinopac_2023 project.
    """

    # Assuming that there is no special trade (deal among acquaintances).
    data["special_trade"] = 0
    # Assuming that there is no "地號" in address.
    data = data.apply(lambda x: isNonTraditionalAddress(x), axis=1)
    data["交易標的"] = "土地+建物"

    return data


def setStreetAndStreetName(houses: pd.DataFrame) -> pd.DataFrame:
    houses = houses.replace("-1", "")
    houses = houses.apply(lambda x: getStreetAndStreetName(x), axis=1)

    return houses


def getStreetAndStreetName(x: pd.Series) -> pd.Series:
    if x["Street"] == "" and x["Section"] == "" and x["Village"] != "":
        x["Street"] = x["Village"]
        x["StreetName"] = x["Village"]
    elif x["Street"] != "":
        x["StreetName"] = x["Street"] + x["Section"]

    return x
