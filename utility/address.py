import pandas as pd
from utility.constants import defaultString, countyAlphabets


def isNonTraditionalAddress(x: pd.Series) -> pd.Series:
    """
    If 地號 appears in address, it may indicate that it is waiting for renewal or
    it is a pre-sale house.
    """

    try:
        x["non_traditional_address"] = 1 if "地號" in x["Address"] else 0
    except TypeError:
        x["non_traditional_address"] = 0

    return x


def extractAddress(address: str, target: str) -> str:
    """
    Extract street name, section, etc from address (for governmental data).
    If county is not provided, yet district ends with 市, then there will be
    mis-classification.

    Parameters:
        address: full address
        target: which part of the address to extract, county, district or street

    Return:
        extracted part
    """

    if target not in ["all", "county", "district", "village", "street", "section"]:
        print("ERROR: target not allowed.")
        return ""

    countyOptions = ["縣", "市"]
    villageOptions = ["里", "村"]
    streetOptions = ["街", "路", "道"]
    sectionOptions = ["段"]

    county = defaultString
    for opt in countyOptions:
        try:
            countyIndex = address[:4].index(opt)
            # In case input address starts from District but also contains "市".
            if address[: countyIndex + 1] in countyAlphabets.values():
                county = address[: countyIndex + 1]
                address = address[countyIndex + 1 :]
                break
        except ValueError:
            continue

    # Handle when 市、區 both exist in address.
    municipalities = ["臺北市", "桃園市", "新北市", "新竹市", "臺中市", "臺南市", "高雄市"]
    allCounties = list(countyAlphabets.values())
    nonMuni = list(set(allCounties) - set(municipalities))
    if county in municipalities:
        districtOptions = ["區"]
    elif county in nonMuni:
        districtOptions = ["市", "鄉", "鎮"]
    elif county == defaultString:
        districtOptions = ["鄉", "鎮", "市", "區"]
    else:
        print("County not in available names.")
        return defaultString

    district = defaultString
    for opt in districtOptions:
        try:
            distrcitIndex = address.index(opt)
            district = address[: distrcitIndex + 1]
            address = address[distrcitIndex + 1 :]
            break
        except ValueError:
            continue

    village = defaultString
    for opt in villageOptions:
        try:
            villageIndex = address.index(opt)
            village = address[: villageIndex + 1]
            address = address[villageIndex + 1 :]
            break
        except ValueError:
            continue

    street = defaultString
    for opt in streetOptions:
        try:
            streetIndex = address.index(opt)
            street = address[: streetIndex + 1]
            address = address[streetIndex + 1 :]
            break
        except ValueError:
            continue

    section = defaultString
    for opt in sectionOptions:
        try:
            sectionIndex = address.index(opt)
            section = address[: sectionIndex + 1]
            address = address[sectionIndex + 1 :]
            break
        except ValueError:
            continue

    if target == "all":
        return county, district, village, street, section
    elif target == "county":
        return county
    elif target == "district":
        return district
    elif target == "village":
        return village
    elif target == "street":
        return street
    elif target == "section":
        return section
    else:
        return defaultString
