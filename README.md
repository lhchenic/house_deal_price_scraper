# Environment
MacOS 10.15.7  
openssl@3  
Python3.9.18  

## Dependencies
 - tor

### Python dependencies
- ipykernel==6.25.2  
- pycurl==7.45.2  
To install pycurl properly, run the following command:  
`export PYCURL_SSL_LIBRARY=openssl && export CPPFLAGS=-I/usr/local/opt/openssl@3/include && export LDFLAGS=-L/usr/local/opt/openssl@3/lib &&  pip install --compile --config-settings=="--with-openssl" --no-cache-dir pycurl==7.45.2 --use-pep517`
- beautifulsoup4==4.12.2  
- numpy==1.26.1
- pandas==2.1.1

# References
- [How to Use cURL in Python](https://scrape-it.cloud/blog/curl-in-python)
- [Extracting text from HTML file using Python](https://stackoverflow.com/a/24618186)
- [Scraping Dynamically Loaded Content with Selenium and BeautifulSoup](https://medium.com/@robertsevan/scraping-dynamically-loaded-content-with-selenium-and-beautifulsoup-2cb7b067ea6c)